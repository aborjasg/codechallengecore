﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;
using WebApp.Data;
using WebApp.Utils;
using Microsoft.Extensions.Configuration;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        public readonly AppSettings _appSettings;
        private readonly SystemAuditContext _context;
        private readonly Utils.ServiceMethods _method;

        public enum ServiceResult : int
        {
            OK = 1,
            NONE = 0,
            ERR = -1
        }

        public HomeController(IConfiguration configuration, SystemAuditContext context)
        {
            _context = context;
            _appSettings = configuration.GetSection("AppSettings").Get<Utils.AppSettings>(); ;
            _method = new Utils.ServiceMethods();
        }

        public IActionResult Index(ServiceInput model)
        {
            var res = ServiceResult.NONE;
            string message = "";

            try
            {
                if (ModelState.IsValid)
                {
                    if (!string.IsNullOrEmpty(model.latitude) && !string.IsNullOrEmpty(model.longitude))
                    {
                        string uriApi = _appSettings.ServiceEndpoint + _appSettings.ServiceResource;
                        message = model.result = _method.getServiceResponse(model, uriApi);
                        res = ServiceResult.OK;

                        // Event Log:
                        _context.EventLog.Add(new Models.EventLog() { vAction = "GET", vInput = model.cql_filter, vOutput = message, iResult = (int)res, dProcessTimestamp = DateTime.UtcNow, vClientInfo = "" });
                        _context.SaveChanges();
                    }
                    else
                    {
                        model.result = "";
                    }
                }
            }
            catch (Exception ex)
            {
                model.result = "[!] Error";
                message = ex.Message;
                res = ServiceResult.ERR;

                // Event Log:
                _context.EventLog.Add(new Models.EventLog() { vAction = "GET", vInput = model.cql_filter, vOutput = message, iResult = (int)res, dProcessTimestamp = DateTime.UtcNow, vClientInfo = "" });
                _context.SaveChanges();
            }

            return View("Index", model);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
