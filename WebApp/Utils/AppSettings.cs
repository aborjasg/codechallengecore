﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Utils
{
    public class AppSettings
    {
        public string AppVersion { get; set; }
        public string ServiceEndpoint { get; set; }
        public string ServiceResource { get; set; }

    }
}
