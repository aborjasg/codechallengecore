﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebApp.Utils
{
    public class ServiceMethods
    {
        public ServiceMethods() 
        {

        }

        public  string getServiceResponse(Models.ServiceInput model, string uriApi)
        {
            try
            {   
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(uriApi);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.Timeout = new TimeSpan(0, 0, 0, 90);

                    string uriString = uriApi + string.Format("?service={0}&version={1}&request={2}&typeName={3}&srsname={4}&cql_filter={5}&propertyName={6}&outputFormat={7}", model.service, model.version, model.request, model.typeName, model.srsname, model.cql_filter, model.propertyName, model.outpuFormat);
                    var responseTask = client.GetAsync(uriString);
                    responseTask.Wait();

                    var response = responseTask.Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var jsonData = (response.Content.ReadAsStringAsync()).Result;
                        Models.ServiceOutput result = JsonConvert.DeserializeObject<Models.ServiceOutput>(jsonData);

                        foreach (var item in result.features)
                        {
                            if (item.properties != null)
                            {
                                return item.properties.CMNTY_HLTH_SERV_AREA_NAME;
                            }
                        }
                        throw new Exception("[!] Invalid data structure!");
                    }
                    else
                    {
                        throw new Exception("[!] Wrong Response!");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }
}
