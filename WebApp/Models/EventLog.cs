﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace WebApp.Models
{
    public class EventLog
    {
        public int Id { get; set; }

        [Display(Name  ="Action")]
        public string vAction { get; set; }

        [Display(Name = "Input")]
        public string vInput { get; set; }

        [Display(Name = "Output")]
        public string vOutput { get; set; }

        [Display(Name = "Result")]
        public int iResult { get; set; }

        [DataType(DataType.Date), Display(Name ="Timestamp")]
        public DateTime dProcessTimestamp { get; set; }

        [Display(Name = "Client Info")]
        public string vClientInfo { get; set; }
    }
}
