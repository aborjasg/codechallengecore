﻿using System;
using Xunit;
using WebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Microsoft.Extensions.Options;
using WebApp;
using Microsoft.AspNetCore.Mvc.Testing;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace UnitTests
{
    public class UnitTest_ServiceMethods
    {
        private readonly string _uriApi = "https://openmaps.gov.bc.ca/geo/pub/ows";

        public UnitTest_ServiceMethods ()
        {
            
        }

        [Fact]
        public void SendRequest_Downtown()
        {            
            var obj = new ServiceInput() { latitude = "+48.4251378", longitude = "-123.3646335" };           
            var result = new WebApp.Utils.ServiceMethods().getServiceResponse(obj, _uriApi);
            
            Assert.Equal("Downtown Victoria/Vic West", result);
        }

        [Fact]
        public void SendRequest_Cowichan()
        {
            var obj = new ServiceInput() { latitude = "+48.8277", longitude = "-123.711" };
            var result = new WebApp.Utils.ServiceMethods().getServiceResponse(obj, _uriApi);

            Assert.Equal("Central Cowichan", result);
        }

        [Fact]
        public void SendRequest_Sooke()
        {
            var obj = new ServiceInput() { latitude = "+48.4251378", longitude = "-123.711" };
            var result = new WebApp.Utils.ServiceMethods().getServiceResponse(obj, _uriApi);

            Assert.Equal("Sooke", result);
        }

        [Fact]
        public void SendRequest_Pender()
        {
            var obj = new ServiceInput() { latitude = "+48.8277", longitude = "-123.3646335" };
            var result = new WebApp.Utils.ServiceMethods().getServiceResponse(obj, _uriApi);

            Assert.Equal("Pender/Galiano/Saturna/Mayne", result);
        }
    }
}
